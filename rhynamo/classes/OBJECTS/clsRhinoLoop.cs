﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino.FileIO;
using Rhino.Geometry;
using Rhino.Geometry.Collections;
using Rhino.Collections;

namespace CASE.classes
{
  class clsRhinoLoop
  {
    public List<Rhino.Geometry.Curve> LoopCurves;

    public clsRhinoLoop()
    {
      LoopCurves = new List<Rhino.Geometry.Curve>();
    }
  }
}
